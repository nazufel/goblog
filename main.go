package main

import (
	"fmt"
	"net/http"

	"github.com/julienschmidt/httprouter"
	"gitlab.com/nazufel/goblog/controllers"
)

func init() {

	// Check the environment for necessary variables.
	checkENV()
}
func main() {

	// Init router
	r := httprouter.New()

	// Init UserController
	uc := controllers.NewUserController(getSession())

	// Alert the server is running to the console
	fmt.Println("Server Running!")

	//########################//
	//### User Controllers ###//
	//########################//

	// CREATE
	r.POST("/users/", uc.CreateUser)
	r.PUT("/users/", uc.CreateUser)
	// READ
	r.GET("/users/", uc.GetUsers)
	r.GET("/users/:id", uc.GetUser)
	// UPDATE
	r.PATCH("/users/:id", uc.UpdateUser)
	r.PUT("/users/:id", uc.UpdateUser)
	r.POST("/users/:id", uc.UpdateUser)
	// DELETE
	r.DELETE("/users/:id", uc.DeleteUsers)

	// Start the server
	http.ListenAndServe("localhost:3000", r)
}
