package controllers

// comment
// more comments
import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
	"time"

	mgo "github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
	"github.com/julienschmidt/httprouter"
	"gitlab.com/nazufel/goblog/models"
)

// UserController represents the controller for operating on the User resource
type UserController struct {
	// update the controller methods so they can access mongo
	session *mgo.Session
}

//NewUserController represents the controller for updating new User Resources
func NewUserController(s *mgo.Session) *UserController {
	// init mongo
	return &UserController{s}
}

// ################### //
// Get important ENV's //
// ################### //

// Get the application's stage: dev, test, stage, or prod from the environment
var appENV = os.Getenv("goBlogENV")

//Get the database name. It will be a combination of stage and DB
var appDB = os.Getenv("goBlogDB")

// Get the collection
var appCollection = os.Getenv("goBlogUserCollection")

// ############# //
// CRUD HANDLERS //
// ############# //

// CREATE: POST, PUT

//TODO: verify username and email are unique

//CreateUser Controller for creating a new user
func (uc UserController) CreateUser(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
	// Init the UserController and use ENV variables to control DB and Collection
	// DB naming convention is a mixture of the stage i.e.: dev, test, prod and the app name, speparated by an underscore
	col := uc.session.DB(appENV + "_" + appDB).C(appCollection)
	// Stub a user to be populated from the body
	us := models.Users{}

	// Populate the user data
	json.NewDecoder(r.Body).Decode(&us)

	//Set user created time
	us.Created = time.Now().Local()

	//Set user updated time
	us.Updated = time.Now().Local()

	// Write the user document to mongo
	err := col.Insert(us)
	if err != nil {
		log.Print(err)
	}

	// Write content-type and return statuscode and original payload
	w.Header().Set("Content-Type", "application/json")
	//http.Redirect(w, r, path+sid, http.StatusMovedPermanently) //302
	w.WriteHeader(http.StatusCreated) //201
	//TODO page redirect to user dashboard
}

// READ: GET //

//GetUser retrieves an individual user resource by username
func (uc UserController) GetUser(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
	// Init the UserController and use ENV variables to control DB and Collection
	// DB naming convention is a mixture of the stage i.e.: dev, test, prod and the app name, speparated by an underscore
	c := uc.session.DB(appENV + "_" + appDB).C(appCollection)

	// Grab username
	id := p.ByName("id")

	// Init the user model interface
	us := models.Users{}

	// TODO: need to retrive user by username, not objectID
	// Fetch user using the objectID
	if err := c.FindId(id).One(&us); err != nil {
		w.WriteHeader(http.StatusNotFound) //404
		fmt.Println("looking for " + id + " failed")
		return
	}

	uj, err := json.Marshal(us)
	if err != nil {
		fmt.Println(err)
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK) // 200
	fmt.Fprintf(w, "%s\n", uj)
}

//GetUsers returns all users in the collection
func (uc UserController) GetUsers(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
	// Init the UserController and use ENV variables to control DB and Collection
	// DB naming convention is a mixture of the stage i.e.: dev, test, prod and the app name, speparated by an underscore
	col := uc.session.DB(appENV + "_" + appDB).C(appCollection)

	// init slice of users to store all user structs returned
	var users []models.Users

	// Fetch all users
	// TODO: limit return to 25, filtered Alphabetically.
	err := col.Find(bson.M{}).All(&users)
	if err != nil {
		w.WriteHeader(http.StatusNotFound) //404
		return
	}

	uj, err := json.Marshal(users)
	if err != nil {
		fmt.Println(err)
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(models.Users{})
	fmt.Fprintf(w, "%s\n", uj)
}

// UPDATE - PATCH, POST, PUT methods

//UpdateUser Controller for updating user document
func (uc UserController) UpdateUser(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
	// Init the UserController and use ENV variables to control DB and Collection
	// DB naming convention is a mixture of the stage i.e.: dev, test, prod and the app name, speparated by an underscore
	col := uc.session.DB(appENV + "_" + appDB).C(appCollection)

	// Init user model - probably could be pulled out as a global package variable.
	us := models.Users{}

	// Setup Decoder and decode HTTP request body into User struct
	json.NewDecoder(r.Body).Decode(&us)

	// Grab id from the router
	id := p.ByName("id")

	//Set user updated time
	us.Updated = time.Now().Local()

	// Write updates to DB
	col.Update(bson.M{"_id": id}, bson.M{"$set": bson.M{"firstname": us.FirstName, "lastname": us.LastName,
		"passwordhash": us.PasswordHash, "email": us.Email, "role": us.Role, "bio": us.Bio,
		"avatar": us.Avatar, "updated": us.Updated}})
	uj, _ := json.Marshal(us)

	// Write content-type and return statuscode and original payload
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusAccepted) //202
	fmt.Fprintf(w, "%s", uj)
}

// DELETE - DELETE method

// DeleteUsers removes an existing user resource DELETE
func (uc UserController) DeleteUsers(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
	// Init the UserController and use ENV variables to control DB and Collection
	// DB naming convention is a mixture of the stage i.e.: dev, test, prod and the app name, speparated by an underscore
	col := uc.session.DB(appENV + "_" + appDB).C(appCollection)

	// TODO: Delete user by username, not object ID
	// get the user id from the httprouter parameter
	id := p.ByName("id")

	// delete user
	err := col.RemoveId(id)
	if err != nil {
		w.WriteHeader(http.StatusNotFound) //404
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
}
