package models

// Note: I have not implimented the controller for this yet. Just mocking what the data strcuture could look like.

//Comments struct to hold comment data
type Comments struct {
	// Comments represents the structure of the resource, using bson to store in mongo
	// ObjectId uses mongo's id service to assign a user id
	User      string `json:"user" bson:"user"`
	Email     string `json:"email,omitempty" bson:"email,omitempy"`
	Subscribe bool   `json:"subscribe" bson:"subscribe"`
	Body      string `json:"body" bson:"body"`
	// TODO: be able add multiple categories to one post
	//Categories []bytes       `json:"categories" bson:"categories"`
	// TODO: be able to add multiple comments from people who are logged in from oAuth.
	// TODO: impliment oAuth, store users in DB, and be able reference user documents from article/comment documents,
}
