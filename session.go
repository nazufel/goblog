package main

import (
	"os"

	mgo "github.com/globalsign/mgo"
)

//GetSession method controls the connection to the data base
func getSession() *mgo.Session {
	// Using ENV variable to control where the app looks for the db
	dbHost := os.Getenv("goBlogDBHost")
	// Using ENV variable for the DB user
	dbUser := os.Getenv("goBlogDBUser")
	// Using ENV variable for DB Password
	dbPassword := os.Getenv("goBlogDBPassword")
	// Connect to the DB by getting secrets and configs from the environment
	s, err := mgo.Dial("mongodb://" + dbUser + ":" + dbPassword + "@" + dbHost)
	//defer s.Close()
	// Check if connection error, is mongo running?
	if err != nil {
		panic(err)
	}
	//defer s.Close()
	s.SetMode(mgo.Monotonic, true)
	return s
}
