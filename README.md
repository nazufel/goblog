# Goblog API
Backend api practice for a website and blog. It's powered by Go and uses MongoDB for a database.

## Repository Organization

1. **controllers** - This directory is also a package of the same name. It contains all the files that define methods that execute CRUD operations against various resources under the app's control.
    - users.go - This file holds the methods for CRUD operations on the users resource.
* **models** - This directory is also a package of the same name. It contains files that define the data structures various resources under the app's control.
    - articles.go - Defines the data structure for the articles resource.
    - comments.go - Defines the data structure for the comments resource.
    - users.go - Defines the data structure for the users resource.
* **vendor** - This directory is controlled by [dep](https://github.com/golang/dep) that's used for dependency management of the app.


## Technical Implementation

### Acceptable Methods
This API is a CRUD app. The following are the acceptable HTTP Request Methods. The methods can be executed against whatever the resource is: ```/users/:id``` or ```/articles/:permalink``` and using a method. ```Comments``` are inserted as a sub-document in an array in the Articles resource. Methods used on comments are ```/articles/comment/:permalink```.

1. Create
    - PUT
    - POST
* Read
    - GET
* Update
    - PATCH
    - PUT
    - POST
* Delete
    - DELETE

### Endpoint Layout

```
// GET all users. Only one method applied here and that's GET
host/users/ - returns all users, will apply filters later

// Where the real CRUD happens
host/users/:id
```

## Environment Variables
The app gets some configuration options from the environment. To run the app the following variables need to be exported:
1. ```goBlogDBHost``` - The IP or DNS resolvable hostname of the server, load balancer, or proxy for the database.
* ```goBlogENV``` - The environment the app is currently running in i.e.: ```dev```, ```test```, ```stage```, ```prod```, etc. Also part of defining the database name.
* ```goBlogDBPort``` - Port of the DB.
* ```goBlogDB``` - The name of the database to use. Convention is to name the database the same name as the service coupled with the environment the app is running in i.e.: ```dev_goblog```.
* ```goBlogUserCollection``` - Collection for a particular set of controllers to use. This environment variable is for the users collection that holds the users.

If any one of these variables the app will panic and fail.

## Data Structure

### Collections

1. Articles
* Users
* Sessions

## List of Must Haves (in no particular order)

* Work on the user model returned
* **DONE** (8/28/18) - Create session package to control dialing to db.
* **DONE** (8/29/18) - Use variables to configure the app.
    - Also fails if an environment variable doesn't exist.
* **DONE** (8/30/18) - Use the username as the id in the DB
* Rudimentary login system with salted hashes. Using oAuth will come later.
* CRUD of Blog article resources - Sort of implemented.
* Serving of static pages: ```About```, ```Contact Us```, ```index```, ```site map```.
* Need to use cookies, properly by warning users and have them opt in, but off by default.
* Wrap page loads in DB transactions - not sure if this is totally necessary, but the [Go Buffalo](https://gobuffalo.io/en) project does this.
* String validation of all type-able fields to avoid scripts and DB comments from being submitted (not sure if this is done server side or client side).
* Filtering of results: limit - ```?limit=10```, sort - ```?sortby=name&order=asc```, etc. More filtering will be addressed as needed.
* **DONE** - Limit endpoints to a specific resource or collection of resources. Good API first design uses [nouns](https://www.youtube.com/watch?v=sMKsmZbpyjE), not verbs.
* Responding with status codes and custom errors where needed
* URL redirection and returning permalinks
* Containerize the app
* **DONE** (8/30/18) - Change dependency from Niemeyer's MGO to [Globalsign](https://github.com/globalsign/mgo).
* Unit Tests: Handler and routes
* Documentation such as [Swagger](https://github.com/go-swagger/go-swagger)
* Handle time. Probably write to DB in UTC, but allow the front end to look up user timezone and adjust UTC to local time when presenting. Will have to think about how to handle global times.
* Add users to mailing list, if they opt in, when they create an account to comment
* Uses Docker with multi-stage builds
* Search field with character scrubbing
* Create Index for Titles and Multi-key Index for Tags of posts
* Robots.txt file
