package main

import (
	"log"
	"os"
)

// Check for all the necessary environment variables to run the app, or fail and log.
func checkENV() {

	// Check if the goBlogDBHost variable is exported to the environment. If not, log and fail.
	dbHost := os.Getenv("goBlogDBHost")
	if len(dbHost) == 0 {
		log.Fatal("Clould not find goBlogDBHost variable. Please export goBlogDBHost to the IP or DNS hostname of the DB server or load balancer.")
	}

	// Check if the goBlogENV variable is exported to the environment. If not, log and fail.
	env := os.Getenv("goBlogENV")
	if len(env) == 0 {
		log.Fatal("Clould not find goBlogENV variable. Please export goBlogENV to the proper stage of development/release, i.e.: dev, test, stage, prod, etc. and run the app again.")
	}

	// Check if the goBlogDB variable is exported to the environment. If not, log and fail.
	db := os.Getenv("goBlogDB")
	if len(db) == 0 {
		log.Fatal("Clould not find goBlogDB variable. Please export goBlogDB to the proper database name, usually the name of the app.")
	}

	// Check if the goBlogDBUser variable is exported to the environment. If not, log and fail.
	dbUser := os.Getenv("goBlogDBUser")
	if len(dbUser) == 0 {
		log.Fatal("Clould not find goBlogDBUser variable. Please export goBlogDBUser to inform the service of what user to use for the DB.")
	}

	// Check if the goBlogDBPassword variable is exported to the environment. If not, log and fail.
	dbPassword := os.Getenv("goBlogDBPassword")
	if len(dbPassword) == 0 {
		log.Fatal("Clould not find goBlogDBPort variable. Please export goBlogDBPassword to inform the service of what password to use for the DB.")
	}

	// Log and print that the variables are exported and the app can continue.
	log.Println("All variables are exported. Starting the server.")

	//return err

}
